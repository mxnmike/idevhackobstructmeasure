//
//  FirstViewController.swift
//  ObstructedMeasurement
//
//  Created by Kevin Le, Miguel Garcia, Lyneth Peou, Prem Cherukuri, Hafsa Naciye on 3/26/16.
//  Copyright © 2016 HouHackathon. All rights reserved.
//

import UIKit
import MapKit

class FirstViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    @IBAction func markHere(sender: UIBarButtonItem) {
        var inputTextField: UITextField?
        let passwordPrompt = UIAlertController(title: "Enter location name", message: "Enter a short name for this location.", preferredStyle: UIAlertControllerStyle.Alert)
        passwordPrompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: nil))
        passwordPrompt.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            if let tbc = self.tabBarController as? MainTabBarViewController {
                tbc.namedLocations.append(NamedLocation(locationName: (inputTextField?.text)!,
                    latitude:(self.userLocation!.coordinate.latitude),
                    longitude:(self.userLocation!.coordinate.longitude)))
            }
            // Now do whatever you want with inputTextField (remember to unwrap the optional)
        }))
        passwordPrompt.addTextFieldWithConfigurationHandler({(textField: UITextField!) in
            textField.placeholder = "Location name"
            inputTextField = textField
        })
        
        presentViewController(passwordPrompt, animated: true, completion: nil)
    }

    @IBOutlet weak var mapView: MKMapView!
    let locationManager = CLLocationManager()
    var userLocation:CLLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = kCLDistanceFilterNone
            locationManager.startUpdatingLocation()
            
            self.mapView.showsUserLocation = true;
            let location = locationManager.location
            let region = MKCoordinateRegionMakeWithDistance(location!.coordinate, 2000, 2000)
            mapView.setRegion(region, animated: true)
            
        }

        
        
//        locationManager.startUpdatingLocation()
//        mapView.showsUserLocation = true
    }

//    func mapView(mapView: MKMapView!, didUpdateUserLocation
//        userLocation: MKUserLocation!) {
//            mapView.centerCoordinate = userLocation.location!.coordinate
//            userLoc = userLocation;
//            let region = MKCoordinateRegionMakeWithDistance(
//                userLoc!.location!.coordinate, 2000, 2000)
//            
//            self.mapView.setRegion(region, animated: true)
//            
//    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
//        let center = CLLocationCoordinate2D(latitude: (location?.coordinate.latitude)!,
//                                            longitude: (location?.coordinate.longitude)!)
        
//        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
//        let region = MKCoordinateRegionMakeWithDistance(location!.coordinate, 2000, 2000)
//        mapView.setRegion(region, animated: true)
        //locationManager.stopUpdatingLocation()
        userLocation = location
    }
    
    func locationManager(manager:CLLocationManager, didFailWithError error:NSError) {
        print("Error " + error.localizedDescription)
    }
}

