//
//  SecondViewController.swift
//  ObstructedMeasurement
//
//  Created by Kevin Le, Miguel Garcia, Lyneth Peou, Prem Cherukuri, Hafsa Naciye on 3/26/16.
//  Copyright © 2016 HouHackathon. All rights reserved.
//

import UIKit
import MapKit

class SecondViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    
    var namedLocations = [NamedLocation]()
    var from:Int?
    var to:Int?
    
    @IBOutlet weak var tableView: UITableView!
    let textCellIdentifier = "TextCell"
    let swiftBlogs = ["Ray Wenderlich", "NSHipster", "iOS Developer Tips", "Jameson Quave", "Natasha The Robot", "Coding Explorer", "That Thing In Swift", "Andrew Bancroft", "iAchieved.it", "Airspeed Velocity"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if let tbc = self.tabBarController as? MainTabBarViewController {
            namedLocations = tbc.namedLocations
        }
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return namedLocations.count
        //return swiftBlogs.count
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            namedLocations.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(textCellIdentifier)
        
        let row = indexPath.row
        //cell.textLabel?.text = swiftBlogs[row]
        cell!.textLabel?.text = namedLocations[row].locationName
        
        if row == from || row == to {
            cell!.accessoryType = .Checkmark
        } else {
            cell!.accessoryType = .None
        }
        
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if from == nil && to == nil {
            from = indexPath.row
            if let cell = tableView.cellForRowAtIndexPath(indexPath) {
                cell.accessoryType = .Checkmark
            }
        } else if to == nil {
            to = indexPath.row
            if let cell = tableView.cellForRowAtIndexPath(indexPath) {
                cell.accessoryType = .Checkmark
            }
        } else {
            let oldIndexPath = NSIndexPath(forRow: from!, inSection: 0)
            if let cell = tableView.cellForRowAtIndexPath(oldIndexPath) {
                cell.accessoryType = .None
            }
            from = to
            to = indexPath.row
            if let cell = tableView.cellForRowAtIndexPath(indexPath) {
                cell.accessoryType = .Checkmark
            }
        }
    }
    
    func directMetersFromCoordinate(from:CLLocationCoordinate2D, to:CLLocationCoordinate2D) -> Double {
        
        let DEG_TO_RAD = 0.017453292519943295769236907684886
        let EARTH_RADIUS_IN_METERS = 6372797.560856
        let latitudeArc  = (from.latitude - to.latitude) * DEG_TO_RAD
        let longitudeArc = (from.longitude - to.longitude) * DEG_TO_RAD
        var latitudeH = sin(latitudeArc * 0.5)
        
        latitudeH = latitudeH * latitudeH
        var lontitudeH = sin(longitudeArc * 0.5)
        lontitudeH = lontitudeH * lontitudeH
        let tmp = cos(from.latitude*DEG_TO_RAD) * cos(to.latitude*DEG_TO_RAD)
        
        return EARTH_RADIUS_IN_METERS * 2.0 * asin(sqrt(latitudeH + tmp*lontitudeH))
        
    }

    @IBAction func measure(sender: AnyObject) {
        if from != nil && to != nil {
            if let fromLat = namedLocations[from!].latitude,
                fromLong = namedLocations[from!].longitude,
                toLat = namedLocations[to!].latitude,
                toLong = namedLocations[to!].longitude {
                let from = CLLocationCoordinate2D(
                    
                    latitude: fromLat,
                    longitude: fromLong
                )
                let to = CLLocationCoordinate2D(
                    latitude: toLat,
                    longitude: toLong
                )
                
                let dist = directMetersFromCoordinate(from, to:to)
                let message = "distance \(dist)"
                let alertCtrl = UIAlertController(title: "Distance", message: message, preferredStyle: .Alert)
                
                let OKAction = UIAlertAction(title: "Continue", style: .Default) {
                    (action:UIAlertAction!) in
                }
                alertCtrl.addAction(OKAction)
                presentViewController(alertCtrl, animated: true, completion: nil)
            }
            
        }
    }
}

