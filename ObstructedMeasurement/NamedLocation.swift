//
//  LocationPoint.swift
//  ObstructedMeasurement
//
//  Created by Kevin Le, Miguel Garcia, Lyneth Peou, Prem Cherukuri, Hafsa Naciye on 3/26/16.
//  Copyright © 2016 HouHackathon. All rights reserved.
//

import Foundation

class NamedLocation {
    var locationName:String?
    var latitude:Double?
    var longitude:Double?
    
    init(locationName:String, latitude:Double, longitude:Double) {
        self.locationName = locationName
        self.latitude = latitude
        self.longitude = longitude
    }
}